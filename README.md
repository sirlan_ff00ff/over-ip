# over ip

because why not shout funny sounds into the void

live at https://over-ip.dragon-vi.be

## usage

have deno installed https://deno.land/manual/getting_started/installation

start the project with

```
deno task start
```

this will watch the project directory and restart as necessary.

### customization

to change the _protocols_ edit `/data/protocols.ts`

add your _protocol_ name to `allProtocols`, and add its rules to `allowedText`
(key is _protcol_ name, then a function that checks a string and returns a boolean if
it meets the criteria)

## info

code is for this project licensed under the _beerware-license revision 42 ramen bowl patch_ (this
project also has no warranty, don't use it to run a nuclear power plant or something)

[![Made with Fresh](https://fresh.deno.dev/fresh-badge-dark.svg)](https://fresh.deno.dev)
