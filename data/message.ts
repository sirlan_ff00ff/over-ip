
export const messsageSotre = 'messages'

export const oldestMessage = 10 *60000 // in minutes
//export const oldestMessage = .5 *60000 // in minutes

export interface Transform {
	x:   number, // top left        0,0
	y:   number, // bottom right 100,100?
	rot: number, // degress
	scale: number,
}

export interface Message {
	content: string,
	from: string,
	date: Date,
	color?: string,
	uuid?: string,
	transform: Transform
}

/* [min, max] */
export const allowedValues = {
	transform: {
		x: [5, 95],
		y: [7, 80],
		rot: [-45, 45],
		scale: [64, 175]
	}
}

export const verifyValues = (message :Message) :boolean => {
	const within = (n :number, min :number, max :number) :boolean => (n >= min-1) && (n <= max+1)

	return [
		within(message.transform.x, allowedValues.transform.x[0], allowedValues.transform.x[1]),
		within(message.transform.y, allowedValues.transform.y[0], allowedValues.transform.y[1]),
		within(message.transform.rot, allowedValues.transform.rot[0], allowedValues.transform.rot[1]),
		within(message.transform.scale, allowedValues.transform.scale[0], allowedValues.transform.scale[1]),
	].reduce((a, b) => a && b)
}

export type MessageList = Array<Message>

export interface MessageData {
	uuid: string,
	protocol: string,
	message: Message
}

export const msgAge = (m: Message, parse = true) => {
	const now = new Date()
	let then : Date
	if (parse) then = new Date(m.date)
	else then = m.date
	return now.getTime() - then.getTime()
}
