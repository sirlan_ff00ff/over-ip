
export type APIMode
	= 'fetch'
	| 'send'

export interface API {
	mode: APIMode,
	// deno-lint-ignore no-explicit-any
	data: any,
}
