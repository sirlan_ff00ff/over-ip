import { effect } from "@preact/signals"
import { Message, MessageData, messsageSotre, oldestMessage, verifyValues, msgAge } from "./message.ts"
import { protocols, allowedText } from "./protocols.ts"
import { chunk, verifyChars } from "./utils.ts"

const kvPath = './localKv'
const kv = await Deno.openKv(kvPath)

const maxMessages = 512
const maxPerprotocol = Math.floor(maxMessages / 4)

export const getMessages = async (protocol: string) :Promise<{data: Array<MessageData>, ok: boolean}> => {
	const messageResults :MessageData[] = []
	const iter = kv.list<Message>({ prefix: [messsageSotre, protocol] }, { limit: maxPerprotocol })
	for await (const res of iter)
		if (msgAge(res.value) <= oldestMessage) {
		messageResults.push({
			uuid: res.key[2] as string,
			protocol: protocol,
			message: res.value
		})
	}
	const sorted = messageResults.toSorted((a, b) => {
		const aTime = new Date(a.message.date).getTime()
		const bTime = new Date(b.message.date).getTime()
		return bTime - aTime
	})
	return { data: sorted, ok: true }
}

export const sendMessage = async (message: Message, protocol: string) :Promise<{data: string | null, ok: boolean}> => {
	const uuid = crypto.randomUUID()
	const messageData :MessageData = {
		message: message,
		protocol: protocol,
		uuid: uuid,
	}
	
	if (!verifyChars(message.from))    return { data: 'your username has illegal chars', ok: false }
	if (!verifyChars(message.content)) return { data: 'the text has illegal chars', ok: false }
	if (!allowedText[protocol](message.content)) return { data: 'the text is not allowed', ok: false }
	if (!verifyValues(message))        return { data: 'wrong data on (transform) values', ok: false }

	let msgCount = 0
	const iter = kv.list({ prefix: [messsageSotre, protocol] })
	for await (const _ of iter) msgCount += 1
	if (msgCount + 1 > maxPerprotocol) {
		return { data: 'too many messages', ok: false }
	}

	//console.log('storing in [' + messsageSotre + ', ' + protocol + ', ' + messageData.uuid + ']')
	const res = await kv.set([messsageSotre, protocol, messageData.uuid], messageData.message)

	if (res.ok) return {data: uuid, ok: true}
	else return { data: "error pushing to kv store", ok: false }
}

const update = async () => {
	const now = new Date()
	// -- delete oldest messages --
	//console.log('trying to clean up')
	// deno-lint-ignore no-explicit-any
	const msgList :Array<any> = []
	// deno-lint-ignore no-explicit-any
	let msgLstList :any[][] = []
	for (const protocol of protocols) {
		const iter = kv.list({ prefix: [messsageSotre, protocol] }, { limit: maxPerprotocol * 2 })
		for await (const res of iter) msgList.push({
			uuid: res.key[2] as string,
			protocol: protocol,
			msgTime: (res.value as Message).date
		})
	}
	msgLstList = chunk(10, msgList)
	for (const lstChunk of msgLstList) {
		const opeation = kv.atomic()
		let opHead = opeation
		for (const msg of lstChunk) {
			const then = new Date(msg.msgTime)
			const timeDiff = now.getTime() - then.getTime()
			if (timeDiff > oldestMessage) opHead = opHead.delete([messsageSotre, msg.protocol, msg.uuid])
		}
		const opRes = await opHead.commit()
		if (!opRes.ok) console.warn('cleanup failed')
		//console.log('clean up status is ' + (opRes.ok ? 'ok' : 'not ok'))
	}
}
effect(() => {
	const interval = setInterval(update, 60_000)
	//const interval = setInterval(update, 10_000)
	return () => clearInterval(interval)
})

