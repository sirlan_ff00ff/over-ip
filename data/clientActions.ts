import { Message, allowedValues } from "./message.ts"
import { APIMode } from "./api.ts"
import { randNum } from "./utils.ts"

// deprecated
// deno-lint-ignore no-explicit-any
const apiFetch = async (url :string, mode :APIMode, data :any) => {
	return await fetch(url, {
		method: 'POST',
		headers: {
			"Content-Type": "application/json",
		},
		body: JSON.stringify({
			mode: mode,
			data: data
		})
	})
}

export const genMessage = (msg: string, user :string) :Message => {
	const av = allowedValues.transform
	return {
		content: msg,
		from: user,
		date: new Date(),
		transform: {
			x: randNum(av.x[0], av.x[1]),
			y: randNum(av.y[0], av.y[1]),
			rot: randNum(av.rot[0], av.rot[1]),
			scale:randNum(av.scale[0], av.scale[1]),
		}
	}
}

// deprecated
export const sendMessage = async (newMessage :Message, name :string) => {
	try {
		const res = await apiFetch('/api/message', 'send', {name: name, message: newMessage})
		const returned = await res.json()
		//console.log("sendMessage result")
		//console.debug(returned)
		if (!(returned.status.ok)) console.log(returned.status)
		return returned
	} catch (_) {
		console.warn("couldn't send message to server")
	}
}

// deprecated
export const getMessages = async (name: string) => {
	const res = await apiFetch('/api/message', 'fetch', {name: name})
	if (!res.ok) { console.warn("couldn't fetch from server") ; return [] }
	const returned = await res.json()
	if ('messages' in returned) {
		// deno-lint-ignore no-explicit-any
		returned.messages = returned.messages.map((m :any) => {
			return {
				content: m.content,
				from: m.from,
				date: new Date(m.date),
				color: m.color,
				transform: m.transform
			}
		})
	}
	//console.log("getMessages result")
	//console.debug(returned)
	return returned
}
