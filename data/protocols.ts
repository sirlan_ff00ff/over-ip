import { hasChar, hasCharOnly, starts, ends } from "./utils.ts"

const allProtocols = [
	'rawr',
	'yip',
	'beep',
]

export const protocols = allProtocols

interface TextAllowList {
	[key: string]: (_ :string) => boolean
}

export const allowedText :TextAllowList = {
	rawr: (_t) => {
		const t = _t.toLowerCase()
		const chars = 'raweou'
		return (t.length >= 3)
			&& hasChar(t, chars)
			&& hasCharOnly(t, chars)
			&& starts(t, ['r', 'wr', 'ar', 'aw'])
			&& ends(t, ['w', 'r'])
	},
	yip: (_t) => {
		const t = _t.toLowerCase()
		const chars = 'yipaeou'
		return (t.length >= 3)
			&& hasChar(t, chars)
			&& hasCharOnly(t, chars)
			&& starts(t, ['y'])
			&& ends(t, ['p'])
	},
	beep: (_t) => {
		const t = _t.toLowerCase()
		const chars = 'bepaioul'
		return (t.length >= 3)
			&& hasChar(t, chars)
			&& hasCharOnly(t, chars)
			&& starts(t, ['b'])
			&& ends(t, ['p'])
	},
}
