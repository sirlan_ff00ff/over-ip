import { Message, MessageData } from "./message.ts";

export const socketPath = "/api/socket"

export interface Socket {
	socket: WebSocket,
	protocol: string,
}

export const webSockets :Map<string, Socket> = new Map()

export interface ServerSocketMessage {
	packetType: 'newMessage'|'delMessage'|'messageList'|'connectionData'|'sendData'
	sendStatus?: 'ok'|'notOk'
	data: MessageData|MessageData[]|string
}
export interface ClientSocketMessage {
	packetType: 'newMessage'
	message: Message
	localId?: string
}

interface BroadcastOptions {
	protocol: string,
	skipSelf?: boolean,
	selfId?: string,
}

export const broadcast = (msg :ServerSocketMessage, options? :BroadcastOptions) => {
	webSockets.forEach((sock, id) => {
		if (options?.protocol)
			if (sock.protocol !== options.protocol)
				return
		if (options?.skipSelf)
			if (id === options.selfId)
				return
		sock.socket.send(JSON.stringify(msg))
	})
}

interface WSHandlers {
	// deno-lint-ignore no-explicit-any
	onOpen?    :(_:Event, id:string) => any,
	// deno-lint-ignore no-explicit-any
	onMessage? :(_:MessageEvent, id:string) => any,
	// deno-lint-ignore no-explicit-any
	onClose?   :(_:CloseEvent, id:string) => any,
	// deno-lint-ignore no-explicit-any
	onError?   :(_:Event|ErrorEvent, id:string) => any,
}

export const initSocket = (
	socket :WebSocket,
	protocol :string,
	handlers = {} as WSHandlers,
) => {
	const uuid = crypto.randomUUID()
	socket.onopen = (e) => {
		webSockets.set(uuid, {socket, protocol})
		return  handlers.onOpen
			? handlers.onOpen(e, uuid)
			: (_:Event) => {}
	}
	socket.onmessage = (e) => handlers.onMessage
		? handlers.onMessage(e, uuid)
		: (_:Event) => {}
	socket.onclose = (e) => {
		webSockets.delete(uuid)
		return handlers.onClose
			? handlers.onClose(e, uuid)
			: (_:Event) => {}
	}
	socket.onerror = (e) => {
		webSockets.delete(uuid)
		return handlers.onError
		? handlers.onError(e, uuid)
		: (_:Event) => {}
	}
}

export const socketUri = (path :string) :string => {
	const l = window.location
	return (l.protocol === "https:" ? 'wss://' : 'ws://')
		+ l.host + path
}
