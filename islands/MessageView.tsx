import { Message, MessageData, MessageList, msgAge, oldestMessage } from "../data/message.ts"
import { signal, Signal } from "@preact/signals"
import { useEffect } from "preact/hooks"
import { clamp } from "../data/utils.ts"
import { useSocket } from "./WSManager.tsx"
import { ServerSocketMessage } from "../data/socket.ts";

interface MessageViewProps {
	protocol: string
	defaultData?: MessageData[]
}

const messages : Signal<MessageList> = signal([
	/*{ content: 'yip'
	, from:    'sirlan'
	, date:    new Date()
	, color:   'ff00ff'
	, transform: {x: 25, y: 25, rot: 15, scale: 110}
	},*/
])
const currentTime = signal(new Date())

/* the message you try to send will 99% of the time be right, so setting it
   wiithout the fetch doesn't change much, just makes it smoother
*/
export const localUpdateMessages = (msg: Message) => {
	messages.value = [...(messages.value), msg]
}


const timeDiff = (then :Date) :number => 
	oldestMessage - (currentTime.value.getTime() - then.getTime())

const diffToPercent = (n :number) :number =>
	clamp(Math.floor((n/oldestMessage) * 100), 0, 100)

export default function MessageView(props :MessageViewProps) {
	useEffect(() => {
		if (props.defaultData) props.defaultData.forEach((msd) => {
			const m = msd.message
			m.uuid = msd.uuid
			m.date = new Date(m.date)
			localUpdateMessages(m)
		})

		useSocket((s) => s.addEventListener("message", (e) => {
			const serverMessage :ServerSocketMessage = JSON.parse(e.data)
			if (!serverMessage.packetType) return
			switch (serverMessage.packetType) {
			case 'newMessage': {
				const messageData :MessageData = serverMessage.data as MessageData
				const m = messageData.message
				m.date = new Date(m.date)
				m.uuid = messageData.uuid
				localUpdateMessages(m)
			; break}
			default: /**/
			}
		}))

		const shortUpdate = () => {
			currentTime.value = new Date()
		}
		const longUpdate = () => {
			messages.value = messages.value.filter((m) => msgAge(m) <= oldestMessage)
		}
		const sInterval = setInterval(shortUpdate, 1_050)
		const lInterval = setInterval(longUpdate, 60_000)
		return () => { clearInterval(sInterval) ; clearInterval(lInterval) }
	}, [])
	return <>
		{messages.value.map((msg, _) =>
		<div
			class="message fade
			       text-4xl font-black"
			msg-id={msg.uuid ? msg.uuid : '???'}
			style={{
				top: msg.transform.y + 'vh',
				left: (msg.transform.x - msg.content.length / 1) + 'vw',
				opacity: clamp(diffToPercent(timeDiff(msg.date)+7), 0,100) + '%',
			}}
		>
			<span class="message-content" title={msg.from +' at '+ msg.date.toLocaleTimeString()}
				style={{
					transform: 'scale('+msg.transform.scale+'%) rotate('+msg.transform.rot+'deg)',
					filter: 'blur('+(
						(100 - clamp(diffToPercent(timeDiff(msg.date)) + 25, 0,100)) * 0.0475
					)+'px)',
				}}
			>{
				msg.content
			}</span>
			<div class="tooltip"
			>
				{msg.from} at {msg.date.toLocaleTimeString()} <br />
				{/*diffToPercent(timeDiff(msg.date))*/}
			</div>
		</div>
		)}
		<script>{`
		`}</script>
		<style>{`
			@keyframes appear-msg {
				0% {
					filter: blur(5px);
					transform: scale(50%);
				}
				100% {
					filter: blur(0px);
					transform: scale(100%);
				}
			}
			@keyframes appear-msg-content {
				0% {
					opacity: 0%;
				}
				100% {
					opacity: 100%;
				}
			}
			.message {
				position: absolute;
				user-select: none;
				transition: opacity 3s linear, filter 3s linear;
				animation: appear-msg ease-in-out 1s ;
			}
			.message-content {
				font-family: Archivo;
				display: inline-block;
				text-shadow: 0 0 4px #6366F1;
				transition: all .25s ease-in;
				color: #e2e6f5;
				animation: appear-msg-content ease-in-out .75s;
			}
			.message-content:hover {
				color: #ffffff;
			}
			.message .tooltip {
				visibility: hidden;
				width: 120px;
				background: #55555570;
				color: #fafaff;
				font-size: 11pt;
				font-weight: normal;
				text-align: center;
				border-radius: 10px;
				padding: .17vmin;
				line-height: 1.5rem;
				position: absolute;
				z-index: 1000;
				bottom: 125%;
				left: 50%;
				margin-left: -60px;
				opacity: 0;
				transition: all .3s ease-in-out;
			}
			.message .tooltip::after {
				content: "";
				position: absolute;
				top: 100%;
				left: 50%;
				margin-left: -5px;
				border-width: 5px;
				border-style: solid;
				border-color: #55555570 transparent transparent transparent;
			}
			.message-content:hover ~ .tooltip {
				visibility: visible;
				opacity: 1;
			}
		`}</style>
	</>
}
