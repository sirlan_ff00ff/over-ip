import { signal } from "@preact/signals"
import { allowedText } from "../data/protocols.ts"
import { genMessage } from "../data/clientActions.ts"
import { useSocket } from "./WSManager.tsx"

interface BarProps {
	protocol: string
	userName: string
}

const maxLength = 25

const text = signal('')
const textOk = signal(true)
const verifyText = (protocol: string) => {
	const t = text.value
	if (t.length > maxLength) { textOk.value = false; return }
	if (t.length < 1) { textOk.value = true; return }
	// verify
	textOk.value = allowedText[protocol](t)
	//textOk.value = true
}

const send = (protocol :string, userName :string) => {
	if (text.value.length < 1) { return }
	if (!textOk.value) return
	const message = genMessage(text.value, userName)
	//localUpdateMessages(message)
	//sendMessage(message, protocol)
	useSocket((s) => s.send(JSON.stringify({packetType:'newMessage', message})))
	text.value = ''
}

export default function TextBar(props: BarProps) {

	return <div
		class="flex items-center justify-center fixed bottom-0 md:bottom-5 w-full"
		//style="position: fixed; bottom: 5vmin; width: 100vw"
		>
		<input
			type="text" value={text} placeholder={props.protocol}
			// deno-lint-ignore no-explicit-any
			onInput={(e:any) => { text.value = e.target.value; verifyText(props.protocol) }}
			// deno-lint-ignore no-explicit-any
			onKeyPress={(e:any) => { if (e.keyCode == 13) send(props.protocol, props.userName) }}
			class="p-7 text-blue-900"
			style={{
				border: !textOk.value ? '3px solid red' : 'none',
				'border-right': 'none',
			}}
		/>
		<button
			onClick={() => send(props.protocol, props.userName)}
			class="flex justify-center items-center
			      bg-gradient-to-b from-blue-600 to-purple-800
			      hover:from-blue-300 hover:to-purple-600
			      transition-all duration-75"
			style={{
			}}
		>
			<svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 512 512" style="transform: scale(140%)">
				<style>{"svg{fill:#f6f5f4}"}</style><path d="M498.1 5.6c10.1 7 15.4 19.1 13.5 31.2l-64 416c-1.5 9.7-7.4 18.2-16 23s-18.9 5.4-28 1.6L284 427.7l-68.5 74.1c-8.9 9.7-22.9 12.9-35.2 8.1S160 493.2 160 480V396.4c0-4 1.5-7.8 4.2-10.7L331.8 202.8c5.8-6.3 5.6-16-.4-22s-15.7-6.4-22-.7L106 360.8 17.7 316.6C7.1 311.3 .3 300.7 0 288.9s5.9-22.8 16.1-28.7l448-256c10.7-6.1 23.9-5.5 34 1.4z"/>
			</svg>
		</button>
		<style>{`
			input {
				border-radius: 50vmin 0 0 50vmin;
				height: 4rem; width: 32vw;
				transition: width .75s, border-radius .75s;
			}
			button {
				border-radius: 0 50vmin 50vmin 0;
				height: 4rem; width: 4rem;
				transition: width .75s, border-radius .75s;
			}
			@media (max-width: 767px) {
				input {
					border-radius: 0;
					width: 100%;
				}
				button {
					border-radius: 0;
					width: 4rem;
				}
			}
		`}</style>
	</div>

}
