import { Signal, signal } from "@preact/signals"
import { useEffect } from "preact/hooks"
import { socketPath, socketUri } from "../data/socket.ts"

interface WSManagerProps {
	protocol: string
}

const sock :Signal<WebSocket|null> = signal(null)
// deno-lint-ignore no-explicit-any
export const useSocket = (fn :(_:WebSocket) => any) :any => {
	if (sock.value === null) return
	return fn(sock.value)
}

export default function WSManager({ protocol } :WSManagerProps) {
	useEffect(() => {
		const path = socketUri(socketPath)
		const _sock = new WebSocket(path + `?protocol=${protocol}`)
		sock.value = _sock
		return () => _sock.close()
	},[])
	return <></>
}
