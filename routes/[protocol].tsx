
import { Head } from "$fresh/runtime.ts"
import { HandlerContext, Handlers, PageProps } from "$fresh/server.ts"
import { protocols } from "../data/protocols.ts"
import { getCookieFrom } from "../data/cookie.ts"
import WSManager from "../islands/WSManager.tsx"
import TextBar from "../islands/TextBar.tsx"
import MessageView from "../islands/MessageView.tsx"
import { MessageData } from "../data/message.ts";
import { getMessages } from "../data/store.ts"

interface Data {
	pageProtocol: string
	userNameCookie: string|undefined
	defaultData?: MessageData[]
}

export const handler: Handlers<Data> = {
	async GET(req :Request, ctx :HandlerContext<Data>) {
		const protocol = ctx.params.protocol
		
		const cookie = req.headers.get('cookie')
		let usc = undefined
		if (cookie) usc = getCookieFrom('name', cookie)

		if (!protocols.includes(protocol))
			return new Response("", {
				status: 307,
				headers: { Location: "/" },
			})

		const getRes = await getMessages(protocol)
		let messageData = undefined
		if (getRes.ok) messageData = getRes.data

		return await ctx.render({pageProtocol: protocol, userNameCookie: usc, defaultData: messageData})
	},
	POST(_ :Request, _ctx :HandlerContext<Data>) {
		return new Response("", {
			status: 307,
			headers: { Location: "/" },
		})
	},
}

export default function OverIp({data} :PageProps<Data>) {
	const protocol :string = data.pageProtocol
	const usc = data.userNameCookie
	const defaultData = data.defaultData
	return <>
		<Head>
			<title>{protocol} over ip</title>
			<link rel="icon" type="image/x-icon" href="/favicon.png" />
			<link rel="stylesheet" href="/style.css" />
			<link rel="stylesheet" href="/msg-style.css" />
		</Head>
		<WSManager protocol={protocol} />
		<div class="fancy-appear h-screen w-full text-blue-50 flex flex-col overflow-hidden">
			<header class="flex w-full place-content-between">
				<a href="/"
					class="text-indigo-500 bg-blue-200 rounded-full font-bold p-2 m-5 hover:bg-blue-50"
					style="height: max-content; width: max-content; user-select: none; z-index: 2;"
				>
					back
				</a>
				<h1 class="text-4xl font-bold my-5 mx-10 text-blue-200 text-bold">{protocol} over ip</h1>
			</header>
			<div class="overflow-hidden">
				<MessageView protocol={protocol} defaultData={defaultData} />
			</div>
			{ usc ? <TextBar userName={usc} protocol={protocol} /> : '' }
		</div>
	</>
}
