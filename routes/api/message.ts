/* deprecated? */
import { Handlers } from "$fresh/server.ts"
import { API } from "../../data/api.ts"
//import { MessageList } from "../../data/message.ts"
import { getMessages, sendMessage } from "../../data/store.ts"

//const messages :MessageList = [] // for testing

export const handler :Handlers = {
	async POST(_req :Request, _ctx) {
		const req = (await _req.json()) as API
		// deno-lint-ignore no-explicit-any
		const to_return :any = {}
		//console.debug(req)

		switch (req.mode) {
		case 'fetch': {
			const getRes = await getMessages(req.data.name)
			//const getRes = {ok: true, data: messages}
			//console.debug(getRes.data)
			if (getRes.ok) {
				to_return.messages = getRes.data
					.map((md) => md.message)
			} else {
				to_return.messages = []
			}
			break
		}
		case 'send': {
			const message = req.data.message
			const name = req.data.name
			//console.debug(message)
			//console.log('to: '+name)
			const sendStatus = await sendMessage(message, name)
			//{
			//	messages.push(message)
			//}
			//const sendStatus = { data: {}, ok: false }
			to_return.status = sendStatus
			break
		}
		default: {/**/}
		}

		const response = new Response(JSON.stringify(to_return), {
			headers: { "Content-Type": "application/json" }})
		return response
	}
}
