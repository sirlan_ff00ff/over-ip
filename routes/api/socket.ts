import { HandlerContext } from "$fresh/server.ts";
import { sendMessage } from "../../data/store.ts";
import { ClientSocketMessage, ServerSocketMessage, broadcast, initSocket } from "../../data/socket.ts";

const urlData = (url :string) :Map<string,string> => {
	const res :Map<string,string> = new Map()
	url.split('?')[1].split('&').forEach((kv) => {
		const [k, v] = kv.split('=')
		res.set(k, v)
	})
	return res
}

export const handler = (req :Request, _ctx :HandlerContext) :Response => {
	const { socket, response } = Deno.upgradeWebSocket(req)
	const reqProto = urlData(req.url).get("protocol")

	if (!reqProto) return new Response()

	initSocket(socket, reqProto, {
		onOpen: (_, _id) => {
			//console.log(` -> new connection from ${id} [${reqProto}]`)
		},
		onMessage: async (e, id) => {
			const clientMessage :ClientSocketMessage = JSON.parse(e.data)
			if (!clientMessage.packetType) return
			switch (clientMessage.packetType) {
			case 'newMessage': {
				const message = clientMessage.message
				const sendStatus = await sendMessage(message, reqProto)

				if (sendStatus.ok)
					broadcast({
						packetType: 'newMessage',
						data: {
							message:message,
							uuid: sendStatus.data as string,
							name: reqProto }
					}, {protocol: reqProto, selfId: id})
				else
					socket.send(JSON.stringify({
						packetType: 'sendError',
						data: sendStatus.data
					} as ServerSocketMessage))
				//console.log(id, message)
			; break }
			default: /**/
			}
		},
		onClose: (_, _id) => {
			//console.log(` <- closed connection ${id} [${reqProto}]`)
		},
	})
	return response
}
