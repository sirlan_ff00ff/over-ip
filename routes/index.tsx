import { Head } from "$fresh/runtime.ts"
import { HandlerContext, Handlers, PageProps } from "$fresh/server.ts"
import { getCookieFrom } from "../data/cookie.ts"
import { protocols } from "../data/protocols.ts"
import NameSet from "../islands/NameSetter.tsx"
import { oldestMessage } from "../data/message.ts"
import { randItem } from "../data/utils.ts"

const message = `
	now with websockets
`

const motds = [
	'hello world',
	'rawrr ::3',
	'now with 30% less overhead',
	'pet the dragon',
	'uuwuu',
	'kobold to kobold communication'
]

const motd = 'hello world'

interface IndexProps {
	userNameCookie: string|undefined
}

export const handler :Handlers = {
	async GET(req :Request, ctx :HandlerContext) {
		const cookie = req.headers.get('cookie')
		let usc = undefined
		if (cookie) usc = getCookieFrom('name', cookie)

		return await ctx.render({userNameCookie: usc})
	},
}

export default function Home(props :PageProps<IndexProps>) {
	return <>
		<Head>
			<title>over ip</title>
			<link rel="icon" type="image/x-icon" href="/favicon.png" />
			<link rel="stylesheet" href="/style.css" />
		</Head>
		<div class="fancy-appear h-screen w-full flex flex-col items-center place-content-around text-blue-50 text-lg">
			<div>
				<h1 class="text-4xl font-bold m-10 text-center"
					title={'everything fades away in ' + oldestMessage/60000 + ' minutes'}
				>choose your protocol</h1>
				{/*<div class="motd"><span>{randItem(motds)}</span></div>*/}
			</div>
			<ul class="border-2 border-blue-50 p-2 rounded-lg w-48">
				{protocols.map((protocol) => <li class="m-2 text-blue-200 hover:text-blue-50 text-center">
					<a href={protocol}>{protocol} over IP</a>
				</li>)}
			</ul>
			{ props.data.userNameCookie ? '' : <NameSet /> }
			<span>{message}</span>
			<span class="text-blue-200"
			>a weird thing from <a href="https://dragon-vi.be" class="text-blue-50"
				>sirlan</a>, with <a href="https://gitlab.com/sirlan_ff00ff/over-ip">🦕 🍋</a> and 💚</span>
		</div>
		<style>{`
			.motd {
				float: right;
				margin: 0;
				padding: 0;
				position: relative;
				width: max-content;
				top: -3.7rem;
				text-shadow: -2px -2px 5px #3F41A0;
				color: #bbc7f1;
				margin-left: 80%;
				font-size: 17pt;
				transition: all 1s ease-out;
			}
			.motd span {
				display: block;
				transform: rotate(-25deg);
			}
			@media (max-width: 567px) {
				.motd {
					font-size: 15pt;
					right: 1em;
					transform: translateX(85%) rotate(-17deg);
					/*left: 70%;
					top: -3rem;*/
				}
			}
		`}</style>
	</>
}
